# Тестовое задание - регистратура

Фронтенд выполнен используя Angular 8 и Bootstrap 4. Имеются русский и английский переводы.

## Как запустить

`ng serve --configuration=ru`

## Другая документация
- [От Angular](doc/angular.md)
- [От Bitbucket](doc/bitbucket.md)
