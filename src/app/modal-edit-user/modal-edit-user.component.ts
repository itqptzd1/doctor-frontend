import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { User, RawUser, IPersonsName } from '../_models/user';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-modal-edit-user',
  templateUrl: './modal-edit-user.component.html',
  styleUrls: ['./modal-edit-user.component.scss']
})
export class ModalEditUserComponent implements OnInit {
  @Input() user: RawUser;
  userForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public activeModal: NgbActiveModal) { }

  ngOnInit() {
    if (this.user.username === undefined) {
      this.userForm = this.formBuilder.group({
        username:   ['', Validators.required],
        password:   ['', Validators.required],
        first_name: ['', Validators.required],
        surname:    ['', Validators.required],
        patronym:   [''],
      });
    } else {
      this.userForm = this.formBuilder.group({
        username:   [this.user.username, Validators.required],
        password:   [this.user.password, Validators.required],
        first_name: [this.user.name.firstName, Validators.required],
        surname:    [this.user.name.surname, Validators.required],
        patronym:   [this.user.name.patronym],
      });
    }

    this.userForm.valueChanges.subscribe(data => {
      this.user.username = data.username;
      this.user.password = data.password;
      this.user.name = {} as IPersonsName;
      this.user.name.firstName = data.first_name;
      this.user.name.surname = data.surname;
      this.user.name.patronym = data.patronym;
    });
  }

  get f() { return this.userForm.controls; }

}
