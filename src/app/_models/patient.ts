import { User, Roles, IPersonsName } from './user';

export class Patient implements User {
  id: string;
  username: string;
  password: string;
  role = Roles.Patient;
  name: IPersonsName;
  medId: string;
  token?: string;
}
