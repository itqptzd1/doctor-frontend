import { User, IPersonsName, Roles } from './user';

export class Doctor implements User {
  id: string;
  username: string;
  password: string;
  name: IPersonsName;
  role = Roles.Doctor;
  token?: string;
}
