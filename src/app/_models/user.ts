import { inherits } from 'util';

export enum Roles {
  Patient = 'Patient',
  Admin = 'Admin',
  Doctor = 'Doctor',
  Registry = 'Registry'
}

export interface IPersonsName {
  firstName: string;
  surname: string;
  patronym: string;
}

export interface User {
  id: string;
  username: string;
  password: string;
  role: Roles;
  token?: string;
}

export class RawUser implements User {
  id: string;
  username: string;
  password: string;
  role: Roles;
  name: IPersonsName;
  token?: string;
}
