import { Component, OnInit } from '@angular/core';
import { DoctorApiService } from '../_services/doctor.api.service';
import { Doctor } from '../_models/doctor';
import { BehaviorSubject, Observable } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalEditUserComponent } from '../modal-edit-user/modal-edit-user.component';
import { Roles } from '../_models/user';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {
  private doctorsSubject$: BehaviorSubject<Doctor[]> = new BehaviorSubject([]);
  public readonly doctors$: Observable<Doctor[]> = this.doctorsSubject$.asObservable();

  constructor(private modalService: NgbModal, private doctorsApi: DoctorApiService) { }

  ngOnInit() {
    this.updateDoctors();
  }

  editDoctor(doc: Doctor) {
    const modalRef = this.modalService.open(ModalEditUserComponent);
    modalRef.componentInstance.user = doc;
    modalRef.result.then((result: Doctor) => {
      this.doctorsApi.Update(result.id, result).subscribe(data => this.updateDoctors(), error => console.error(error)); // TODO: handle error
    }, reason => {});
  }

  addDoctor() {
    const modalRef = this.modalService.open(ModalEditUserComponent);
    modalRef.componentInstance.user = {role: Roles.Doctor} as Doctor;
    modalRef.result.then((result: Doctor) => {
      this.doctorsApi.Create(result).subscribe(data => this.updateDoctors(), error => console.error(error));
    }, reason => {});
  }

  updateDoctors() {
    this.doctorsApi.GetAll()
    .subscribe(docs =>
      this.doctorsSubject$.next(docs)
    );
  }

}
