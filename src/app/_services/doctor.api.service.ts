import { Injectable, NgZone, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { ConfigService } from 'ng-config-service';
import { User } from '../_models/user';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { Doctor } from '../_models/doctor';

@Injectable({
  providedIn: 'root'
})
export class DoctorApiService {
  private url: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.url = environment.apiUrl;
  }

  // Doctor API

  GetAll(): Observable< Doctor[] > {
    return this.http
      .get<Doctor[]>(`${this.url}/doctor/all`);
  }

  GetOne(id: string): Observable< Doctor > {
    return this.http
      .get<Doctor>(`${this.url}/doctor/${id}`);
  }

  Create(doc: Doctor): Observable<any> {
    return this.http
      .post<Doctor>(`${this.url}/doctor/add`, doc);
  }

  Update(id: string, doc: Doctor): Observable< Doctor > {
    return this.http
      .put<Doctor>(`${this.url}/doctor/${id}`, doc);
  }

  Delete(id: string): Observable<any> {
    return this.http
      .delete<Doctor>(`${this.url}/doctor/${id}`);
  }
}
