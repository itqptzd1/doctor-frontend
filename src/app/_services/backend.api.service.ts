import { Injectable, NgZone, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { ConfigService } from 'ng-config-service';
import { User } from '../_models/user';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BackendApiService {
  private url: string;

  constructor(private http: HttpClient, private config: ConfigService) {
    this.url = environment.apiUrl;
  }

  // User API

  register(user: User): Observable<void> {
    return this.http
      .post<any>(`${this.url}/users/register`, user);
  }
}
